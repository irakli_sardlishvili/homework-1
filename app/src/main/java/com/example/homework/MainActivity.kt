package com.example.homework

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        generate_button.setOnClickListener {
            val randomnumber = generateRandomNumber()
            val YesOrNo =
                    d("generate_number", "the number was generated ${OddOrEven(randomnumber)}")
            random_number.text = randomnumber.toString()
        }
    }

    private fun generateRandomNumber() = (-100..100).random()
    private fun OddOrEven(randomnumber: Int):String {
        if (randomnumber / 5 > 0) {
            if (randomnumber%5==0){
                return "yes"
            }else{
                return "no"
            }
        }else{
            return "no"
        }
    }
}